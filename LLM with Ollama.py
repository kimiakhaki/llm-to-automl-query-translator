import tkinter as tk
from tkinter import ttk
import spacy
import ollama
import threading

# Load the English language model
nlp = spacy.load('en_core_web_sm')

data = {
    'id': [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20],
    'object_type': ['chair', 'table', 'lamp', 'bookshelf', 'vase', 'mirror', 'desk', 'sofa', 'clock', 'cabinet', 
                    'stool', 'bed', 'rug', 'painting', 'basket', 'frame', 'plant', 'speaker', 'statue', 'sculpture'],
    'material': ['wood', 'metal', 'plastic', 'wood', 'ceramic', 'glass', 'wood', 'fabric', 'metal', 'wood', 'wood', 
                 'wood', 'fabric', 'canvas', 'wicker', 'wood', 'clay', 'plastic', 'marble', 'bronze'],
    'color': ['brown', 'purple', 'white', 'brown', 'blue', 'silver', 'brown', 'gray', 'silver', 'brown', 'black', 
              'gray', 'red', 'multi-color', 'brown', 'gold', 'green', 'black', 'white', 'gold']
}


data_columns = ['id', 'object_type', 'material', 'color']
available_materials = list(set(data['material']))
available_colors = list(set(data['color']))
unrelated_question_resonse = "unrelated"

modelfile='''
FROM llama3

# set the temperature to 1 [higher is more creative, lower is more coherent]
PARAMETER temperature 1

# set the system message
SYSTEM """
You are a query parser. You are not supposed to explain anything to the users or have a conversation with them!

We have a database of items with their material and color. Given an input text, you need to determine if it's question related to material, or color of an object, and if so, reformat it into the following structure: 
'Give me items with [adjective value(a value of a color or material)] [adjective(the word material/color based on the previous word)]'. Your input is just the reformatted sentence, nothing more.
If the input is not related to these topics, respond with the following exact word, nothing more: unrelated
Relevant keywords for each category are:
- Material: ["material", "plastic", "metal", "wood", "steel", "cotton"]
- Color: ["color", "red", "blue", "green", "yellow", "golden"]

Your response should focus on the related part of the input. Ignore unrelated parts.

Some examples of valid inputs:
1. "Thanks for you response, now what are the green objects?" -> "items with green color"

Now, parse the input accordingly.
"""
'''

ollama.create(model='example', modelfile=modelfile)


def ask_question():
    user_input = question_entry.get()
    ask_button["state"] = "disabled"
    root.update_idletasks() 

    
    response = ollama.chat(model='example', messages=[{'role': 'user', 'content': user_input}])
    question = response['message']['content']

    if question == unrelated_question_resonse:
        answer = "Sorry, I don't think your question is related to this data."
    else:
        dataset_query = translate_to_dataset_query(question)
        result = filter_dataset(data, dataset_query)
        answer = ", ".join(result) if result else "No matching objects found."

    ask_button["state"] = "normal"
    answer_label.config(text=answer)


def translate_to_dataset_query(question):
    doc = nlp(question)
    dataset_query = []

    for token in doc:
        dataset_query.append(token.text.lower() + '=')

    return ' '.join(dataset_query)


def filter_dataset(data, query):
    filters = [filter_string.strip() for filter_string in query.split('=') if filter_string.strip()]
    key = None
    value = None

    for string in filters:
        if string in data_columns:
            key = string
        elif string in available_materials or string in available_colors:
            value = string

    print(filters)
    print(key)
    print(value)
    filtered_objects = [obj for obj, attr in zip(data['object_type'], data[key]) if attr.lower() == value]

    return filtered_objects


# Create main window
root = tk.Tk()
root.title("Query Translator System")

# Create frame
frame = ttk.Frame(root, padding="20")
frame.grid(row=0, column=0, sticky=(tk.W, tk.E, tk.N, tk.S))

# Question label and entry
question_label = ttk.Label(frame, text="Enter your question:")
question_label.grid(row=0, column=0, sticky=(tk.W, tk.N))

question_entry = ttk.Entry(frame, width=50)
question_entry.grid(row=0, column=1, sticky=(tk.W, tk.E, tk.N, tk.S))

# Ask button
ask_button = ttk.Button(frame, text="Ask", command=ask_question)
ask_button.grid(row=0, column=2, sticky=(tk.W, tk.E, tk.N, tk.S))

# Answer label
answer_label = ttk.Label(frame, text="")
answer_label.grid(row=1, column=0, columnspan=3, sticky=(tk.W, tk.E, tk.N, tk.S))

# Dataset table
tree = ttk.Treeview(frame, columns=data_columns, show="headings")
for col in data_columns:
    tree.heading(col, text=col)
tree.grid(row=2, column=0, columnspan=3, sticky=(tk.W, tk.E, tk.N, tk.S))
for i in range(len(data['id'])):
    values = (data['id'][i], data['object_type'][i], data['material'][i], data['color'][i])
    tree.insert("", "end", values=values)

# Run main loop
root.mainloop()
