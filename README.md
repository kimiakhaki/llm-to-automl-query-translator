# LLM-to-AutoML query translator

## Description
This project utilizes a Language Model (LLM) to analyze user input. If the input is relevant to the database within the project, the LLM attempts to parse it into one of the predefined patterns coded into the system. Subsequently, a series of procedures are executed on the parsed input, aiming to derive an answer from the existing database.
In cases where the input is unrelated to the database, the LLM generates a response indicating its inability to comprehend the input. An example of such a response could be: "Sorry, I couldn't understand your input."

## Installation
To run this project, you need to first install Ollama on your system:

- Go to [Ollama website](https://ollama.com/download) and download the desired version.
- Follow the installation instructions provided for your operating system.

## Usage
- Once Ollama is installed, use the following command to launch the Ollama server:
```
$ ollama serve
```
- After launching the server, you will see something like this:
![Alt text](/Screenshot(934).png "logs")

- Now, simply open the project file and run it as you run any Python file on your system.

## Demo
![Alt text](/Query%20Translator%20Demo.mp4 "Demo")
